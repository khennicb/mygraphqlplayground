package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/khennicb/mygraphqlplayground/graph/generated"
	"gitlab.com/khennicb/mygraphqlplayground/graph/model"
)

func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (*model.Todo, error) {
	var todoAuthor *model.User
	for i, u := range storedUsers {
		if u.ID == input.UserID {
			todoAuthor = &storedUsers[i]
			break
		}
	}

	if todoAuthor == nil {
		return nil, errors.New("user not found")
	}

	lastId++
	addedTodo := model.Todo{
		ID:   strconv.Itoa(lastId),
		Text: input.Text,
		Done: false,
		User: todoAuthor,
	}
	storedTodos = append(storedTodos, addedTodo)

	return &addedTodo, nil
}

func (r *mutationResolver) TickTodo(ctx context.Context, input string) (*model.Todo, error) {
	var todo *model.Todo
	for i, t := range storedTodos {
		if t.ID == input {
			todo = &storedTodos[i]
			break
		}
	}
	if todo == nil {
		return nil, errors.New("todo not found")
	}
	todo.Done = true
	return todo, nil
}

func (r *queryResolver) Todos(ctx context.Context, filter *string) ([]*model.Todo, error) {
	var todos []*model.Todo

	if filter == nil {
		for i := range storedTodos {
			todos = append(todos, &storedTodos[i])
		}
		return todos, nil
	} else {
		for i, t := range storedTodos {
			if *filter == t.ID {
				todos = append(todos, &storedTodos[i])
			}
		}
		return todos, nil
	}
	panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
var storedUsers = []model.User{
	{ID: "1", Name: "User1"},
	{ID: "2", Name: "User2"},
}
var storedTodos = []model.Todo{
	{ID: "1", Text: "my first task", Done: true, User: &storedUsers[0]},
	{ID: "2", Text: "the second one", Done: false, User: &storedUsers[0]},
	{ID: "3", Text: "abc", Done: false, User: &storedUsers[1]},
	{ID: "4", Text: "def", Done: false, User: &storedUsers[0]},
}
var lastId int = 4
